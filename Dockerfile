# Select Base Image
FROM node:18
# FROM node:18-alpine

# Set Environment
ENV NODE_ENV development

# Create app directory
WORKDIR /usr/src/app

# Copy package*.json Files
COPY src/package*.json  .

# RUN npm install && clean cache
RUN npm ci && npm cache clean --force

RUN npm install -g nodemon
# Expose ports
EXPOSE 8080 9229

# Change to non-root User
USER node

# Start Application
CMD [ "nodemon","--inspect=0.0.0.0:9229", "index.js"]
